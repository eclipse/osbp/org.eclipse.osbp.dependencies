<?xml version="1.0" encoding="UTF-8"?>
<!--#======================================================================= -->
<!--# Copyright (c) 2017 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany). -->
<!--# All rights reserved. This program and the accompanying materials -->
<!--# are made available under the terms of the Eclipse Public License 2.0 -->
<!--# which accompanies this distribution, and is available at -->
<!--# https://www.eclipse.org/legal/epl-2.0/ -->
<!--# -->
<!--# SPDX-License-Identifier: EPL-2.0 -->
<!--# -->
<!--# Contributors: -->
<!--# Loetz GmbH&Co.KG - initial API and implementation -->
<!--#======================================================================= -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>org.eclipse.osbp.dependencies</groupId>
		<artifactId>org.eclipse.osbp.dependencies</artifactId>
		<version>0.9.0-SNAPSHOT</version>
		<relativePath>..</relativePath>
	</parent>

	<artifactId>org.eclipse.osbp.dependencies.feature.apache.cxf</artifactId>
	<packaging>eclipse-feature</packaging>

	<properties>
		<cxf-version>3.3.0</cxf-version>
	</properties>

	<repositories>
	</repositories>

	<dependencies>
		<!-- CQ: http://dev.eclipse.org/ipzilla/show_bug.cgi?id=19446 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-core</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-core</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19447 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-bindings-soap</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-bindings-soap</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19448 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-bindings-xml</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-bindings-xml</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19449 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-databinding-jaxb</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-databinding-jaxb</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19450 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxrs</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxrs</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19451 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxws</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-jaxws</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19452 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-simple</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-frontend-simple</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19453 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-client</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-client</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19459 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-security</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-security</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>

		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19454 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-transports-http</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-transports-http</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19455 -->
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-wsdl</artifactId>
			<version>${cxf-version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-wsdl</artifactId>
			<version>${cxf-version}</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=12909 -->
		<dependency>
			<groupId>org.apache.servicemix.bundles</groupId>
			<artifactId>org.apache.servicemix.bundles.wsdl4j</artifactId>
			<version>1.6.3_1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.servicemix.bundles</groupId>
			<artifactId>org.apache.servicemix.bundles.wsdl4j</artifactId>
			<version>1.6.3_1</version>
			<classifier>sources</classifier>
		</dependency>
		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19456 -->
		<dependency>
			<groupId>org.apache.ws.xmlschema</groupId>
			<artifactId>xmlschema-core</artifactId>
			<version>2.2.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.ws.xmlschema</groupId>
			<artifactId>xmlschema-core</artifactId>
			<version>2.2.1</version>
			<classifier>sources</classifier>
		</dependency>

		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19457 -->
		<!-- artefactid: javax.xml.soap -->
		<!-- version: 1.3.0.v201105210645 -->

		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19458 -->
		<!-- artefactid: javax.activation -->
		<!-- version: 1.1.0.v201211130549 -->

		<!-- CQ: https://dev.eclipse.org/ipzilla/show_bug.cgi?id=19462 -->
		<dependency>
		    <groupId>javax.ws.rs</groupId>
		    <artifactId>javax.ws.rs-api</artifactId>
		    <version>2.1</version>
		</dependency>
		<dependency>
		    <groupId>javax.ws.rs</groupId>
		    <artifactId>javax.ws.rs-api</artifactId>
		    <version>2.1</version>
			<classifier>sources</classifier>
		</dependency>


	</dependencies>
	<build>
		<plugins>
			<plugin>
				<groupId>org.eclipse.tycho.extras</groupId>
				<artifactId>tycho-source-feature-plugin</artifactId>
				<version>${tychoExtrasVersion}</version>
				<executions>
					<execution>
						<id>source-feature</id>
						<phase>package</phase>
						<goals>
							<goal>source-feature</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<labelSuffix> (source)</labelSuffix>
					<excludes>
						<plugin id="org.apache.cxf.cxf-core" />
						<plugin id="org.apache.cxf.cxf-rt-bindings-soap" />
						<plugin id="org.apache.cxf.cxf-rt-bindings-xml" />
						<plugin id="org.apache.cxf.cxf-rt-databinding-jaxb" />
						<plugin id="org.apache.cxf.cxf-rt-frontend-jaxws" />
						<plugin id="org.apache.cxf.cxf-rt-frontend-simple" />
						<plugin id="org.apache.cxf.cxf-rt-security" />
						<plugin id="org.apache.cxf.cxf-rt-transports-http" />
						<plugin id="org.apache.cxf.cxf-rt-wsdl" />
						<plugin id="org.apache.cxf.cxf-rt-frontend-jaxrs" />
						<plugin id="org.apache.cxf.cxf-rt-rs-client" />
						<plugin id="org.apache.servicemix.bundles.wsdl4j" />
						<plugin id="org.apache.ws.xmlschema.core" />
						<plugin id="javax.xml.soap" />
						<plugin id="javax.ws.rs-api" />
					</excludes>
				</configuration>
			</plugin>
			<plugin>
				<!-- workaround while bug https://bugs.eclipse.org/bugs/show_bug.cgi?id=398250 
					is not fixed -->
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-p2-plugin</artifactId>
				<version>${tycho-version}</version>
				<executions>
					<execution>
						<id>attached-p2-metadata</id>
						<phase>package</phase>
						<goals>
							<goal>p2-metadata</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>
</project>
